#include <iostream>
#include <vector>

using std::vector;

class TGraph{
    vector<vector<int>> list;
public:
    int dfs(int vertex) {
        vector<int> vertexes;
        vector<int> vertexesColour(list.size(), 0);
        vertexes.push_back(vertex);
        vertexesColour[vertex] = 1;
        while (!vertexes.empty()) {
            int x = vertexes.back();
            vertexes.pop_back();
            for (int i = 0; i < list[x].size(); ++i) {
                if (vertexesColour[list[x][i]] == 0) {
                    vertexes.push_back(list[x][i]);
                    vertexesColour[list[x][i]] = 1;
                }
            }
            vertexesColour[x] = 2;
        }
    }
};