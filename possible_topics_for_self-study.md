# Возможные темы для саморазбота  
## Уже отвеченные темы  
1) Сбалансированные деревья поиска. АВЛ-дерево, красно-черное дерево, операции в них, доказательства 
    сбалансированности.  
    Закодить любое из них.  
2) Декартово дерево(treap). Построение, операции, доказательство единственности возможного дерева 
    для фиксированной последовательности элементов.    
    Закодить  
3) Дерево отрезков. Реализация запросовснизу и сверху. 
   Дерево отрезков для суммы. Дерево отрезков для максимума.   
    Закодить 
## Темы, которые можно брать  
1) Задача о максимальном потоке в графе. Постановка задачи, связанные определения.  
    Алгоритмы Форда-Фалкерсона, Эдмондся-Карпа, Диница. Ассимптотики.   
    Либо закодить, либо заботать с доказательством все связаные теоремы и уметь доказывать корректность алгоритма.  
2) Задача о максимальном потоке в графе. Постановка задачи, связанные определения.  
    Алгоритм проталкивания предпотока. Доказательство корректности работы + ассимптотики.  
    Либо закодить, либо заботать с доказательством все связаные теоремы и уметь доказывать корректность алгоритма.  
3) Хеш-таблица.  
     Хеш-функции. Остаток от деления, мультипликативная. Требования к хеш-функции. Простейшие примеры хеш-функций для чисел и строк.  
    Полиномиальная хеш-функция. Ее использование для строк.  
    Хеш-таблицы. Понятие коллизии.  
    Методы разрешения коллизий: Метод цепочек (открытое хеширование), Метод прямой адресации (закрытое хеширование).  
    Последовательное пробирование. Линейное пробирование. Проблема кластеризации. Квадратичное пробирование.(Последовательный, линейный, квадратичный поиск.)  
    Хеширование кукушкой, Двойное хеширование.  
    Кодить НЕ обязательно, но можно, если берете этот билет.  
4) Длинная арифметика. Основной принцип. Сложение, вычитание.  
    Умножение. Алгоритм Карацубы.  
    Деление.  
    Закодить хотя-бы сложение, вычитание, умножение, для длинных + перевод из обычного в длинное.  
5) Красно-черное дерево, операции в нем, доказательство сбалансированности.   
    Закодить.  
6) Префикс-функция. Тривиальный алгоритм построения. Эффективный алгоритм построения. Решение задачи поиска подстроки 
    в строке с помощью префикс-функции(Алгоритм Кнута-Морриса-Пратта). Закодить нахождение префикс-функции и алгоритм КМП на основе ее.  
7) Z-функция. Тривиальный алгоритм построения. Эффективный алгоритм построения. Решение задачи поиска подстроки в строке с помощью z-функции(Алгоритм Кнута-Морриса-Пратта). Закодить нахождение z-функции и 
    алгоритм КМП на основе ее.  
8) Можно взять пункты 6 и 7, заботать их теоретически + заботать построение префикс функции по z-функции, 
    и, например, строки по z-функции, но ничего не кодить.  
