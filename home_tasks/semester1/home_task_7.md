# Седьмое домашнее задание
## Дедлайн : 20.12.2019 23:59 

1) На основе того, [что было сделано на семинаре](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/seminars/sem_6/graph.cpp) 
	реализовать обход в ширину для графа, и добавить в класс TGraph слудующие методы:
	метод, проверяющий граф на связность, 
	метод, подсичтывающий количество компонент связности в графе,
	метод, проверяющий граф на наличие цикла,
	метод, проверяющий является ли граф деревом. 30б