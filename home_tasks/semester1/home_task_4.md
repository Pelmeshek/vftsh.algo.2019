# Четвертое домашнее задание: Куча + доработки прошлых домашек  
## Дедлайн : 14.11.2019 23:59  

1) Добавить в [табличку](https://docs.google.com/spreadsheets/d/1mwzzSPPt4dPMijqQDHUl8jTttXysjCAV6SSfTK9qUWo/edit?usp=sharing) свои фио, логин битбакета,ссылку на репозиторий и логин в [Я.Контест](https://contest.yandex.ru/contest/15251/participants/)   
2) [Прочитать](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/useful_information/increment.md) про отличия префиксного и постфиксного инкрементов  
3) Заслать прошлые домашки, а если уже засылали, то править замечания, пока все не будет ок. **Задача считается зачтенной только тогда, когда 
    баллы за нее появляются в табличке**. Не забывайте про [кодстайл](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/style_guide.md).  
4) Посмотреть [контест](https://contest.yandex.ru/contest/15251/participants/) и начать решать задачи из него. **Дедлайн по 4 первым задачам из контеста - 28 ноября.**  
5) Реализовать создание кучи через ShiftUp(подъем элемента вверх).  
