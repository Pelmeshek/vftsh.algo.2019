# Первое домашнее задание
## Дедлайн 13.02.2020 23:59
0) [Создать ветку с названием Style_guide](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/how_to_create_branches.md), в ней создать файл со своим кодстайлом, 
    в котором нужно либо прописать свои правила кодстайла, либо то, что вы соглашаетесь на 
    [общий кодстайл прошлого семестра](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/style_guide.md).   
    [Сделать пулл реквест этой ветки в мастер](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/how_to_create_pull_request.md)

1) За О(1) выполнить k-й циклический сдвиг списка.  
    *k-й циклический сдвиг списка* - это список, полученный перестановкой первых k символов в его конец.  
    За какую минимальную асимптотику это можно сделать с вектором?  
    Вам может помочь [ссылка](https://ru.cppreference.com/w/cpp/container/list)
  
2) В программу подается текст. Для заданного текста для каждого слова указать, сколько раз оно встречается в тексте за минимальную возможную ассимптотику.  

3) Вам предлагается запрограммировать базу данных, состоящую из фамилий и имен, с минимальным функционалом. Должны обрабатываться следующие команды:  
    add - добавить *Фамилия Имя* в базу  
    check - проверить, есть ли *Фамилия Имя* в базе, и вернуть True/False  
    delete - удалить *Фамилия Имя* из базы  
    show - вывести все содержимое базы на экран в алфавитном порядке по фамилиям (люди с одинаковыми фамилиями, но разными именами, могут быть выведены в каком угодно порядке)  
    select - вывести всех людей с данной фамилией  
    Напишите за какую ассимптотику работает каждая из этих функций в вашей реализации и почему вы выбрали именно такой вариант реализации  
Пример работы программы: 
***
**Ввод**    
add                    
Просто Санек            
add                      
Батиков Женя            
add                   
Ковальчук Никита  
add  
Просто Гошан  
check  
Горгородов Оксиминог  
delete  
Батиков Женя  
show  
select  
Просто  
*** 
**Вывод**  
False  
Ковальчук Никита  
Просто Санек  
Просто Гошан  
Просто Санек  
Просто Гошан  