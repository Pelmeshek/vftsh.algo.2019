# Выставление оценки за полугодие  
За полугодие вы получите 3 оценки:  

* Оценка за реализацию алгоритмов и структур данных(AlgoMark)
* Оценка за контест(ContestMark)
* Оценка за финальный зачет(ExamMark)
 
Итоговая оценка будет считаться примерно так:

```cpp
double CalculateFinalMark(int AlgoMark, int ContestMark, int ExamMark) {
    if (ExamMark == 2 || AlgoMark == 2) {
        return 2;
    }
    return (1.0/3) * (AlgoMark + ContestMark + ExamMark);
}
```