# Репозиторий курса Алгоритмы и структуры данных ВФТШ #
В папке **[seminars](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/seminars/)** будут выкладываться материалы с занятий  
В папке **[home_tasks](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/home_tasks/)** будут выкладываться домашние задания  
**[C++ style guide или как писать код:](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/style_guide.md)**  
**[Программа устного экзамена](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/exam_program.md)**  

## Материал второго семестра  
**[Консультация к экзамену](https://docs.google.com/spreadsheets/d/1teIaRSuYif3B0zYVTyyuy3bIWgEXBvkrO_9gb2McCwQ/edit#gid=0)**  
**[Контест 2 семестра](https://contest.yandex.ru/contest/17207/)**  
**[Табличка с баллами](https://docs.google.com/spreadsheets/d/1mwzzSPPt4dPMijqQDHUl8jTttXysjCAV6SSfTK9qUWo/edit#gid=146309623)**  
**[Программа к экзаменеу 2 семестра и система оценивания](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/program2sem.md)**  
**[Темы для вопросов по выбору](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/possible_topics_for_self-study.md)**  

## GIT  
**[Создание ветки](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/how_to_create_branches.md)**  
**[Создание пулл-реквеста](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/how_to_create_pull_request.md)**  

## Материал первого семестра  
**[Контест 1 семестра](https://contest.yandex.ru/contest/15251/enter/)**  
**[Табличка с баллами](https://docs.google.com/spreadsheets/d/1mwzzSPPt4dPMijqQDHUl8jTttXysjCAV6SSfTK9qUWo/edit#gid=0)**  
**[Программа курса](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/program.md)**  
**[Выставление оценок](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/grading_scale.md)**  
**[Билеты к экзамену](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/exam_tickets.md)**  

**Полезные ссылки**:  
[Миникурс по основам с++](https://stepik.org/course/16480/syllabus)  
[Изучение git в игровой форме](https://learngitbranching.js.org/). Здесь задачи могут попасьтся сложнее требуемого в вфтш уровня, но штука интересная  